<?php
require_once('conexion.php');
class datos{
    private $db;
	private $res;
	private $mostrar;
    public function __construct(){
		$bd = new conexion();
		$this->db= $bd->getConexion();
		$this->res=array();
		$this->mostrar=array();
    }
	public function registrar($a,$b,$c,$d){
        $consulta=$this->db->prepare("call sp_registra(?,?,?,?)");
		$consulta->bindParam(1,$a);
		$consulta->bindParam(2,$b);
		$consulta->bindParam(3,$c);
		$consulta->bindParam(4,$d);
		$consulta->execute();
		$consulta = null;
		$this->db = null; 
    }
	public function actualiza($a,$b,$c,$d,$e){
        $consulta=$this->db->prepare("call sp_actualiza(?,?,?,?,?)");
		$consulta->bindParam(1,$a);
		$consulta->bindParam(2,$b);
		$consulta->bindParam(3,$c);
		$consulta->bindParam(4,$d);
		$consulta->bindParam(5,$e);
		$consulta->execute();
		$consulta = null;
		$this->db = null; 
    }
	public function elimina($a){
        $consulta=$this->db->prepare("call sp_elimina(?)");
		$consulta->bindParam(1,$a);
		$consulta->execute();
		$consulta = null;
		$this->db = null; 
    }
	public function mostrar(){
        $consulta=$this->db->prepare("call sp_mostrar()");
		$consulta->execute();
		while($filas=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->mostrar[]=$filas;
        }
		return $this->mostrar;
		$consulta = null;
		$this->db = null; 
    }
	public function pais(){
        $consulta=$this->db->prepare("call sp_pais()");
		$consulta->execute();
		while($filas=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->mostrar[]=$filas;
        }
		return $this->mostrar;
		$consulta = null;
		$this->db = null; 
    }
}