drop database mmm;
create database mmm;
use mmm;
create table pais(
id int primary key auto_increment,
nom varchar(45)
);
create table usu(
id int primary key auto_increment,
nom varchar(45),
ape varchar(45),
tel varchar(45),
idp int,
foreign key(idp) references pais(id)
);
delimiter |
create procedure sp_registra(pnom varchar(45),pape varchar(45),ptel varchar(45), pidp int)
begin
insert into usu (nom,ape,tel, idp)values(pnom,pape,ptel, pidp);
end
|
delimiter |
create procedure sp_actualiza(pnom varchar(45),pape varchar(45),ptel varchar(45), pidp int, pid int)
begin
update usu set nom = pnom, ape = pape, tel = ptel, idp = pidp where id = pid;
end
|
delimiter |
create procedure sp_elimina(pid int)
begin
delete from usu where id = pid;
end
|
delimiter |
create procedure sp_mostrar()
begin
select
u.id as 'idusu',
u.nom as 'nomusu',
u.ape as 'apeusu',
u.tel as 'telusu',
u.idp as 'idpais',
p.nom as 'nompais'
from usu u
join pais p
on u.idp = p.id;
end
|
delimiter |
create procedure sp_pais()
begin
select * from pais;
end
|