-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-03-2019 a las 19:01:27
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `l`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualiza`(pnom varchar(45),pape varchar(45),ptel varchar(45), pidp int, pid int)
begin
update usu set nom = pnom, ape = pape, tel = ptel, idp = pidp where id = pid;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_elimina`(pid int)
begin
delete from usu where id = pid;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_mostrar`()
begin
select
u.id as 'idusu',
u.nom as 'nomusu',
u.ape as 'apeusu',
u.tel as 'telusu',
u.idp as 'idpais',
p.nom as 'nompais'
from usu u
join pais p
on u.idp = p.id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pais`()
begin
select * from pais;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_registra`(pnom varchar(45),pape varchar(45),ptel varchar(45), pidp int)
begin
insert into usu (nom,ape,tel, idp)values(pnom,pape,ptel, pidp);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE IF NOT EXISTS `pais` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `nom`) VALUES
(1, 'peru'),
(2, 'venezuela'),
(3, 'ecuador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu`
--

CREATE TABLE IF NOT EXISTS `usu` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `ape` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `idp` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usu`
--

INSERT INTO `usu` (`id`, `nom`, `ape`, `tel`, `idp`) VALUES
(1, 'ana lucia', 'luz', '931510579', 3),
(2, 'ana lucia', 'luz', '931510579', 1),
(3, 'anais', 'ocola', '973111444', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `correo` varchar(90) DEFAULT NULL,
  `contraseña` varchar(90) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `correo`, `contraseña`) VALUES
(1, 'goyo', 'goyo@gmail.com', '1234'),
(2, 'ana', 'ana@gmail.com', '123'),
(3, 'abc', 'abc@gmail.com', '1234'),
(4, 'ccc', 'ccc@gmail.com', '1234'),
(5, 'aaa', 'abc@gmail.com', 'abcd'),
(6, 'ooo', 'ooo@gmail.com', '1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usu`
--
ALTER TABLE `usu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idp` (`idp`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usu`
--
ALTER TABLE `usu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usu`
--
ALTER TABLE `usu`
  ADD CONSTRAINT `usu_ibfk_1` FOREIGN KEY (`idp`) REFERENCES `pais` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
